import sublime, sublime_plugin
import os, sys
import subprocess
import functools
import time

GOPATH = os.getenv("GOPATH")

def get_pkg_name(file_name):
    file_path = os.path.normpath(os.path.dirname(file_name))
    if not file_path.startswith(GOPATH):
        return ""
    file_path = file_path.replace(os.path.normpath(GOPATH + "/src/"), "")
    if len(file_path) > 0 and file_path[0] == os.sep:
        file_path = file_path[1:]
    return file_path.replace(os.sep, "/")

def get_pkg_bin(pkg):
    return os.path.join(os.path.normpath(GOPATH + "/bin/"), os.path.basename(pkg))

def is_test_file(file_name):
    name, ext = os.path.splitext(file_name)
    return name.endswith("_test")

class GoCommand(sublime_plugin.WindowCommand):
    def run(self, cmd = "", pkg = "", file_regex = "", line_regex = "", working_dir = "",
            encoding = "utf-8", env = {}, quiet = False, kill = False,
            # Catches "path" and "shell"
            **kwargs):

        cmdargs = []

        # setup package
        if pkg == "":
            view = self.window.active_view()
            if view == None:
                sublime.error_message("Not implemented yet")
                return
            file_name = view.file_name()
            pkg = get_pkg_name(file_name)

        # setup command
        if cmd == "":
            if pkg.endswith("_test"):
                cmd = "test"
                for region in view.sel():
                    if not region.empty():
                        testname = view.substr(region)
                        if testname.startswith("Benchmark"):
                            cmdargs.append("-bench")
                            cmdargs.append(testname[len("Benchmark"):])
                        elif testname.startswith("Test"):
                            cmdargs.append("-run")
                            cmdargs.append(testname[len("Test"):])
            else:
                cmd = "install"
        elif cmd == "run":
            cmd = "install"
            cmdargs.append("&&")
            cmdargs.append(get_pkg_bin(pkg))

        shell_cmd = "go " + cmd + " " + pkg + " " + " ".join(cmdargs)

        self.window.run_command("exec", {"shell_cmd": shell_cmd, "file_regex": file_regex,
            "line_regex": line_regex, "working_dir": working_dir,
            "encoding": encoding, "env": env, "quiet": quiet, "kill": kill})

class GoEventListener(sublime_plugin.EventListener):
    def on_post_save(self, view):
        pkg = get_pkg_name(view.file_name())
        if pkg == "":
            return

        if os.name == "nt":
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

        process = subprocess.Popen(("gofmt", "-w", view.file_name()),
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, startupinfo=startupinfo)
        print("gofmt -w " + view.file_name())

class GoFmtCommand(sublime_plugin.TextCommand):
    def is_enabled(self):
        return get_pkg_name(self.view.file_name()) != ""

    def run(self, edit):
        self.view.window().run_command("exec", {"cmd": ["gofmt", "-w", self.view.file_name()]})
